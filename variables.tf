variable "prefix" {
  type = string
  description = "The prefix for all generated resources"
  default = "n17-example-aws-sns"
}

variable "notify17_raw_api_key" {
  type = string
  description = "The raw API key to use, to generate raw and raw templated test notifications. If empty, terraform will NOT generate SNS topics for raw and raw templated notifications."
  default = ""
}

variable "notify17_template_api_key" {
  type = string
  description = "The notification template API key to use, to generate templated test notification. If empty, terraform will NOT generate SNS topics for templated notifications"
  default = ""
}

variable "default_tags" {
  type = map(string)
  description = "Any tags to add to all generated resources"
  default = {}
}