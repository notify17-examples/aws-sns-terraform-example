/*

This file contains all AWS resources we need to generate raw notifications from an SNS topic.

https://notify17.net/docs/api-endpoints/#raw-notifications

*/

locals {
  # Only generate resources if there is a raw API key
  generate_test_hook_raw = var.notify17_raw_api_key != ""

  # The SNS endpoint to use
  sns_endpoint_prod_test_raw = "https://hook.notify17.net/api/raw/${var.notify17_raw_api_key}/sns"
}

// --- Raw

resource "aws_sns_topic" "test-hook-raw" {
  count = local.generate_test_hook_raw ? 1 : 0

  name = "${var.prefix}-test-hook-raw"

  tags = merge(var.default_tags, {
    "Name": "${var.prefix}-test-hook-raw",
  })
}

output "test-hook-raw-arn" {
  value = aws_sns_topic.test-hook-raw[*].arn
  description = "The ARN of the generated SNS topic for raw notifications"
}

resource "aws_sns_topic_subscription" "test-hook-raw" {
  count = local.generate_test_hook_raw ? 1 : 0

  topic_arn = aws_sns_topic.test-hook-raw[count.index].arn
  endpoint = local.sns_endpoint_prod_test_raw
  protocol = "https"
  confirmation_timeout_in_minutes = 1
  endpoint_auto_confirms = true
}

resource "local_file" "test-hook-raw" {
  count = local.generate_test_hook_raw ? 1 : 0

  content = replace(
  <<CONTENT
#!/usr/bin/env bash

message="$1"
shift

# This script can be used to test the generation of raw notifications
#
# Example: ./bin/test-hook-raw.sh "This is a message!" --message-attributes '{"sound":{"DataType":"String","StringValue":"doink"}}' --subject "SNS experiment!"

aws sns publish \
  --region ${data.aws_region.current.name}  \
  --topic-arn "${aws_sns_topic.test-hook-raw[count.index].arn}" \
  --message "$message" \
  "$@"
CONTENT
  , "\r", "")

  filename = "${path.root}/bin/test-hook-raw.sh"
  file_permission = "0777"
}

// --- Lambda

data "template_file" "lambda-test-hook-raw" {
  count = local.generate_test_hook_raw ? 1 : 0

  template = file("${path.module}/lambda.js")

  vars = {
    sns_topic_arn = aws_sns_topic.test-hook-raw[count.index].arn
  }
}

resource "local_file" "lambda-test-hook-raw" {
  count = local.generate_test_hook_raw ? 1 : 0

  filename = "${path.module}/.tftmp/lambda-test-hook-raw.js"
  content = data.template_file.lambda-test-hook-raw[count.index].rendered
}

data "archive_file" "lambda-test-hook-raw" {
  count = local.generate_test_hook_raw ? 1 : 0

  type = "zip"
  source_file = local_file.lambda-test-hook-raw[count.index].filename
  output_path = "${path.module}/.tftmp/lambda-test-hook-raw.zip"
}

resource "aws_lambda_function" "lambda-test-hook-raw" {
  count = local.generate_test_hook_raw ? 1 : 0

  filename = data.archive_file.lambda-test-hook-raw[count.index].output_path
  function_name = "${var.prefix}-lambda-test-hook-raw"
  role = aws_iam_role.lambda.arn
  handler = "lambda-test-hook-raw.handler"
  source_code_hash = data.archive_file.lambda-test-hook-raw[count.index].output_base64sha256
  runtime = "nodejs14.x"
}

resource "local_file" "lambda-test-hook-raw-invoke" {
  count = local.generate_test_hook_raw ? 1 : 0

  content = replace(
  <<CONTENT
#!/usr/bin/env bash

payload="$1"
shift

# This script can be used to test the generation of raw notifications via [Lambda -> SNS -> Notify17]
#
# Example: ./bin/lambda-test-hook-raw-invoke.sh '{"name": "Mr. Anderson"}' /dev/null

aws lambda invoke \
  --region ${data.aws_region.current.name} \
  --function-name "${aws_lambda_function.lambda-test-hook-raw[count.index].arn}" \
  --payload "$payload" \
  "$@"
CONTENT
  , "\r", "")

  filename = "${path.root}/bin/lambda-test-hook-raw-invoke.sh"
  file_permission = "0777"
}

output "lambda-test-hook-raw-arn" {
  value = aws_lambda_function.lambda-test-hook-raw[*].arn
  description = "The ARN of the generated Lambda function for raw notifications"
}
