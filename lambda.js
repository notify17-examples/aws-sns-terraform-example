const AWS = require("aws-sdk");

// This lambda function will generate a SNS notification when triggered
exports.handler = (event, context) => {
    const eventText = JSON.stringify(event, null, 2);
    console.log("Received event:", eventText);
    const sns = new AWS.SNS();
    const params = {
        Message: eventText,
        Subject: "SNS/Lambda message!",
        TopicArn: "${sns_topic_arn}"
    };
    sns.publish(params, context.done);
}