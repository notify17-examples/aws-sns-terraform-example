resource "aws_iam_role" "lambda" {
  name = "${var.prefix}-lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# Allows lambda to publish messages to all SNS topics in the account
resource "aws_iam_role_policy" "lambda-all" {
  role = aws_iam_role.lambda.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action" : [
        "sns:Publish"
      ],
      "Effect" : "Allow",
      "Resource" : [
        "arn:aws:sns:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:*"
      ]
    }
  ]
}
EOF
}