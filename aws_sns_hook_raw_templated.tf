/*

This file contains all AWS resources we need to generate raw templated notifications from an SNS topic.

https://notify17.net/docs/api-endpoints/#raw-templated-notifications

*/

locals {
  # Only generate resources if there is a raw API key
  generate_test_hook_raw_templated = var.notify17_raw_api_key != ""

  # The SNS endpoint to use
  sns_endpoint_prod_test_raw_templated = "https://hook.notify17.net/api/raw/${var.notify17_raw_api_key}/sns"
}

// --- Raw templated

resource "aws_sns_topic" "test-hook-raw-templated" {
  count = local.generate_test_hook_raw_templated ? 1 : 0

  name = "${var.prefix}-test-hook-raw-templated"

  tags = merge(var.default_tags, {
    "Name": "${var.prefix}-test-hook-raw-templated",
  })
}

output "test-hook-raw-templated-arn" {
  value = aws_sns_topic.test-hook-raw-templated[*].arn
  description = "The ARN of the generated SNS topic for raw templated notifications"
}

resource "aws_sns_topic_subscription" "test-hook-raw-templated" {
  count = local.generate_test_hook_raw_templated ? 1 : 0

  topic_arn = aws_sns_topic.test-hook-raw-templated[count.index].arn
  endpoint = local.sns_endpoint_prod_test_raw_templated
  protocol = "https"
  confirmation_timeout_in_minutes = 1
  endpoint_auto_confirms = true
}

resource "local_file" "test-hook-raw-templated" {
  count = local.generate_test_hook_raw_templated ? 1 : 0

  content = replace(
  <<CONTENT
#!/usr/bin/env bash

message="$1"
shift

# This script can be used to test the generation of raw templated notifications
#
# Example: ./bin/test-hook-raw-templated.sh "This is a message!" --message-attributes '{"__n17TitleTemplate":{"DataType":"String","StringValue":"My title!"},"__n17ContentTemplate":{"DataType":"String","StringValue":"{{ dump . }}"}}'

aws sns publish \
  --region ${data.aws_region.current.name}  \
  --topic-arn "${aws_sns_topic.test-hook-raw-templated[count.index].arn}" \
  --message "$message" \
  "$@"
CONTENT
  , "\r", "")

  filename = "${path.root}/bin/test-hook-raw-templated.sh"
  file_permission = "0777"
}
