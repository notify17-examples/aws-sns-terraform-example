/*

This file contains all AWS resources we need to generate templated notifications from an SNS topic.

https://notify17.net/docs/api-endpoints/#templated-notifications

*/

locals {
  # Only generate resources if there is a template API key
  generate_test_hook_templated = var.notify17_template_api_key != ""

  # The SNS endpoint to use
  sns_endpoint_prod_test_template = "https://hook.notify17.net/api/template/${var.notify17_template_api_key}/sns"
}

// --- Templated

resource "aws_sns_topic" "test-hook-templated" {
  count = local.generate_test_hook_templated ? 1 : 0

  name = "${var.prefix}-test-hook-templated"

  tags = merge(var.default_tags, {
    "Name": "${var.prefix}-test-hook-templated",
  })
}

output "test-hook-templated-arn" {
  value = aws_sns_topic.test-hook-templated[*].arn
  description = "The ARN of the generated SNS topic for templated notifications"
}

resource "aws_sns_topic_subscription" "test-hook-templated" {
  count = local.generate_test_hook_templated ? 1 : 0

  topic_arn = aws_sns_topic.test-hook-templated[count.index].arn
  endpoint = local.sns_endpoint_prod_test_template
  protocol = "https"
  confirmation_timeout_in_minutes = 1
  endpoint_auto_confirms = true
}

resource "local_file" "test-hook-templated" {
  count = local.generate_test_hook_templated ? 1 : 0

  content = replace(
  <<CONTENT
#!/usr/bin/env bash

message="$1"
shift

# This script can be used to test the generation of templated notifications
#
# Example: ./bin/test-hook-templated.sh "This is a message!" --message-attributes '{"myKey1":{"DataType":"String","StringValue":"Mr. Anderson"}}'

aws sns publish \
  --region ${data.aws_region.current.name}  \
  --topic-arn "${aws_sns_topic.test-hook-templated[count.index].arn}" \
  --message "$message" \
  "$@"
CONTENT
  , "\r", "")

  filename = "${path.root}/bin/test-hook-templated.sh"
  file_permission = "0777"
}

// --- Lambda

data "template_file" "lambda-test-hook-templated" {
  count = local.generate_test_hook_raw ? 1 : 0

  template = file("${path.module}/lambda.js")

  vars = {
    sns_topic_arn = aws_sns_topic.test-hook-templated[count.index].arn
  }
}

resource "local_file" "lambda-test-hook-templated" {
  count = local.generate_test_hook_raw ? 1 : 0

  filename = "${path.module}/.tftmp/lambda-test-hook-templated.js"
  content = data.template_file.lambda-test-hook-templated[count.index].rendered
}

data "archive_file" "lambda-test-hook-templated" {
  count = local.generate_test_hook_raw ? 1 : 0

  type = "zip"
  source_file = local_file.lambda-test-hook-templated[count.index].filename
  output_path = "${path.module}/.tftmp/lambda-test-hook-templated.zip"
}

resource "aws_lambda_function" "lambda-test-hook-templated" {
  count = local.generate_test_hook_raw ? 1 : 0

  filename = data.archive_file.lambda-test-hook-templated[count.index].output_path
  function_name = "${var.prefix}-lambda-test-hook-templated"
  role = aws_iam_role.lambda.arn
  handler = "lambda-test-hook-templated.handler"
  source_code_hash = data.archive_file.lambda-test-hook-templated[count.index].output_base64sha256
  runtime = "nodejs14.x"
}

resource "local_file" "lambda-test-hook-templated-invoke" {
  count = local.generate_test_hook_raw ? 1 : 0

  content = replace(
  <<CONTENT
#!/usr/bin/env bash

payload="$1"
shift

# This script can be used to test the generation of templated notifications via [Lambda -> SNS -> Notify17]
#
# Note, the `-` at the end is needed to print the result to stdout
# Example: ./bin/lambda-test-hook-templated-invoke.sh '{"name": "Mr. Anderson"}' /dev/null

aws lambda invoke \
  --region ${data.aws_region.current.name} \
  --function-name "${aws_lambda_function.lambda-test-hook-templated[count.index].arn}" \
  --payload "$payload" \
  "$@"
CONTENT
  , "\r", "")

  filename = "${path.root}/bin/lambda-test-hook-templated-invoke.sh"
  file_permission = "0777"
}

output "lambda-test-hook-templated-arn" {
  value = aws_lambda_function.lambda-test-hook-templated[*].arn
  description = "The ARN of the generated Lambda function for templated notifications"
}
