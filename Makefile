.PHONY: docs
docs:
	terraform-docs markdown --show-all=false --show inputs,outputs --output-file README.md .