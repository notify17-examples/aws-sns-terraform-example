# The notification template API key to use, to generate raw and raw templated test notifications
#
# If empty, terraform will NOT generate SNS topics for raw and raw templated notifications
notify17_raw_api_key = ""

# The raw API key to use, to generate templated test notifications
#
# If empty, terraform will NOT generate SNS topics for templated notifications
notify17_template_api_key = ""