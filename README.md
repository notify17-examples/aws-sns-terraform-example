# Notify17 AWS SNS terraform example

This project contains a simple example on how to use AWS SNS topics, to generate notifications on Notify17.

## Requirements

* [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)
* An [AWS](https://aws.amazon.com/) account (free tier is ok!)
* A free [Notify17](https://notify17.net) account

## Deployment

1. If you want to use a specific AWS profile/region, please uncomment and replace the values in the [`terraform.tf`](./terraform.tf) file, or prefix the upcoming `terraform` and `./bin` commands with `AWS_PROFILE=my-profile` and/or `AWS_REGION=us-east-1` (e.g. `AWS_PROFILE=my-profile AWS_REGION=us-east-1 terraform init`).
1. [if you want to test raw or raw templated notifications] Create a [raw API key](https://dash.notify17.net/#/rawAPIKeys) in Notify17 to be used with this example, and copy the key to the `terraform.tfvars` file.
1. [if you want to test templated notifications] Create a [notification template](https://dash.notify17.net/#/notificationTemplates) in Notify17 (suggestion: to easily get started, import the [SNS catch-all template](https://notify17.net/use-cases/aws-sns/#catch-all-template)), and copy the key to the `terraform.tfvars` file.
1. Run `terraform init` to initialize the project.
1. Run `terraform apply` and type `yes` when prompted, to generate the AWS resources.
1. Test the hooks using the scripts in the `bin` folder:
    
    * Raw notifications: `./bin/test-hook-raw.sh "This is a message!" --message-attributes '{"sound":{"DataType":"String","StringValue":"doink"}}' --subject "SNS experiment!"`
    * Raw notifications [via Lambda function]: `./bin/lambda-test-hook-raw-invoke.sh '{"name": "Mr. Anderson"}' /dev/null`
    * Raw templated notification: `./bin/test-hook-raw-templated.sh "This is a message!" --message-attributes '{"__n17TitleTemplate":{"DataType":"String","StringValue":"My title!"},"__n17ContentTemplate":{"DataType":"String","StringValue":"{{ dump . }}"}}'`
    * Templated notification: `./bin/test-hook-templated.sh "This is a message!" --message-attributes '{"myKey1":{"DataType":"String","StringValue":"Mr. Anderson"}}'`
    * Templated notification [via Lambda function]: `./bin/lambda-test-hook-templated-invoke.sh '{"name": "Mr. Anderson"}' /dev/null`
   
1. Once you're done, delete the example's resources with `terraform destroy`.
    
<!-- BEGIN_TF_DOCS -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_default_tags"></a> [default\_tags](#input\_default\_tags) | Any tags to add to all generated resources | `map(string)` | `{}` | no |
| <a name="input_notify17_raw_api_key"></a> [notify17\_raw\_api\_key](#input\_notify17\_raw\_api\_key) | The raw API key to use, to generate raw and raw templated test notifications. If empty, terraform will NOT generate SNS topics for raw and raw templated notifications. | `string` | `""` | no |
| <a name="input_notify17_template_api_key"></a> [notify17\_template\_api\_key](#input\_notify17\_template\_api\_key) | The notification template API key to use, to generate templated test notification. If empty, terraform will NOT generate SNS topics for templated notifications | `string` | `""` | no |
| <a name="input_prefix"></a> [prefix](#input\_prefix) | The prefix for all generated resources | `string` | `"n17-example-aws-sns"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_lambda-test-hook-raw-arn"></a> [lambda-test-hook-raw-arn](#output\_lambda-test-hook-raw-arn) | The ARN of the generated Lambda function for raw notifications |
| <a name="output_lambda-test-hook-templated-arn"></a> [lambda-test-hook-templated-arn](#output\_lambda-test-hook-templated-arn) | The ARN of the generated Lambda function for templated notifications |
| <a name="output_test-hook-raw-arn"></a> [test-hook-raw-arn](#output\_test-hook-raw-arn) | The ARN of the generated SNS topic for raw notifications |
| <a name="output_test-hook-raw-templated-arn"></a> [test-hook-raw-templated-arn](#output\_test-hook-raw-templated-arn) | The ARN of the generated SNS topic for raw templated notifications |
| <a name="output_test-hook-templated-arn"></a> [test-hook-templated-arn](#output\_test-hook-templated-arn) | The ARN of the generated SNS topic for templated notifications |
<!-- END_TF_DOCS -->