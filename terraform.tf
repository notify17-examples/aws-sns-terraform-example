terraform {
  # For this example, uses a locally-managed state
  backend "local" {}
}

provider "aws" {
  # Uncomment and edit these variables to force a specific profile and region

  #  profile = "my-profile"

  #  region = "us-east-1"
}
